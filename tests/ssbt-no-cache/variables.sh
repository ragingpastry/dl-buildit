# single-stage build test (ssbt)
export IMAGE="ssbt-no-cache";
export USE_CACHE="false";
export DOCKERFILE="tests/ssbt/Dockerfile";
export TARGETS="";
export CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:=localhost:5000}"
