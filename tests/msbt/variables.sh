# multi-stage build test (msbt)
export IMAGE="msbt";
export TARGETS="base wget curl final";
export DOCKERFILE="tests/${IMAGE}/Dockerfile";
export CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:=localhost:5000}"
