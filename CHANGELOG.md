# [1.4.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.3.0...v1.4.0) (2019-10-31)


### Features

* Add support for passing --build-args ([bdc761e](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/bdc761e))

# [1.3.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.2.0...v1.3.0) (2019-10-29)


### Features

* Add more testing and input validation ([10b6ae5](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/10b6ae5))

# [1.2.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.1.0...v1.2.0) (2019-10-23)


### Features

* Add USE_CACHE option ([4f52093](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/4f52093))

# [1.1.0](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.0.1...v1.1.0) (2019-10-22)


### Features

* Add BuildKit support ([d15b6c2](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/d15b6c2))

## [1.0.1](https://gitlab.com/dreamer-labs/repoman/dl-buildit/compare/v1.0.0...v1.0.1) (2019-10-21)


### Bug Fixes

* Added additional debugging messaging to buildit ([0151d24](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/0151d24))

# 1.0.0 (2019-10-21)


### Features

* initial commit ([75a22af](https://gitlab.com/dreamer-labs/repoman/dl-buildit/commit/75a22af))
